**Gitee的Markdown文本展示好像有点问题，请移步我的博客——[个人博客系统整体介绍](https://blog.csdn.net/Stone__Fly/article/details/114019537?spm=1001.2014.3001.5501)** 
# 1、写在前面
其实现在可以写博客的在线网站很多，有CSDN，有简书，甚至连GitHub都能写博客，而且还有[Wordpress](https://blog.csdn.net/Stone__Fly/article/details/110930012)可以轻松构建个人博客网站，不过还是想弄个自己的个人博客网站，从头到尾自己搞的那种，就当是熟练下写项目的技能吧。于是就写了这个PersonalBlog系统。
# 2、项目简介
## 2.1 功能罗列
**PersonalBlog**是用Java开发的个人博客系统，我开发这个项目最初的打算是先出一个原型，然后再不断改进，而原型的功能就参考了[B站教程-SpringBoot博客](https://www.bilibili.com/video/BV1KJ411D7bW)，在教程中，作者以管理员和用户两个角色的需求罗列了系统所必须的功能，在这些功能上添加了点个人思考，做了下面这个**功能的思维导图**：
![功能罗列](https://img-blog.csdnimg.cn/20210304214304849.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1N0b25lX19GbHk=,size_16,color_FFFFFF,t_70)
**后端管理**包含三大块内容：博客的增删改查、分类的增删改查以及标签的增删改查。
**前端展示**主要负责博客的各种展示：所有博客展示、最新博客展示、按照分类展示以及按照标签展示等。
我相信好的博客用户看了肯定会想要说点什么，因此**评论功能**是必不可少的，这点教程作者跟大多数个人博客的作者一样，我称之为**非注册式评论**，就是用户无需注册，只需要提供昵称和邮箱，即可对博客进行评论，但是我觉得这对后续回复评论不是很友好，所以这里我采用**注册式评论**，用户需要先注册登录一个账号才能进行评论，同时这也是我的一点期望，打算以后能在个人博客上添加站内邮件的功能，或者即时通讯。
## 2.2 技术框架
开发PersonalBlog所采用的**技术框架**如下：
- 前端：Semantic UI框架【由于是原型，前端页面直接拿[教程源码](https://gitee.com/chengfeng99/SpringBootBlog)里的来用，后期有好的前端模板再修改即可】
- 后端：SpringBoot + SpringSecurity + MyBatis + thymeleaf模板
- 数据库：MySQL8.0

其实咱们这个博客系统是个小项目，用不着SpringSecurity，不过我想着平常上班用不到这个框架，就加上去练练手，说实话，要不是一边百度一边写项目，我还真搞不定这玩意儿。
## 2.3 开发工具
- IDEA
- Maven 3
- JDK 8
# 3、启动教程
## 3.1 源码导入
我的源码地址：[https://gitee.com/AlaGeek/PersonalBlog](https://gitee.com/AlaGeek/PersonalBlog)
工具安装我就不多说了，这里默认大家已经会用上述开发工具了，首先大家先把源码下载下来，因为我是用IDEA开发的，大家可以直接用IDEA打开，然后自行修改Maven地址为你们本地的Maven地址：
![Maven地址修改](https://img-blog.csdnimg.cn/20210304221920241.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1N0b25lX19GbHk=,size_16,color_FFFFFF,t_70)
如果JDK对不上，也自行修改：
![JDK地址修改](https://img-blog.csdnimg.cn/2021030422204530.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1N0b25lX19GbHk=,size_16,color_FFFFFF,t_70)
## 3.2 环境安装
项目启动需要连接MySQL，如果大家电脑上没有安装MySQL，可以参考这篇博客——[使用Docker搭建MySQL服务](https://www.cnblogs.com/sablier/p/11605606.html)，安装的时候请安装MySQL8.0以上的版本。
安装完MySQL8.0后，在如下目录，修改文件application-dev.properties中MySQL连接地址。
![文件目录](https://img-blog.csdnimg.cn/20210304223546926.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1N0b25lX19GbHk=,size_16,color_FFFFFF,t_70)
有了MySQL后，运行下列SQL脚本，生成对应数据库、表结构以及数据：
```sql
create database personal_blog;

use personal_blog;

drop table if exists `user`;
create table `user`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`account` varchar(60) not null comment "账号",
	`password` varchar(255) not null comment "密码",
	`nickname` varchar(60) not null comment "昵称",
	`email` varchar(100) not null comment "邮箱",
	`picture` varchar(500) not null default("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3317240047,2073069235&fm=11&gp=0.jpg") comment "头像",
	`enabled` tinyint(1) not null default(1) comment "用户状态",
	`create_time` datetime not null comment "创建时间",
	`update_time` datetime not null comment "更新时间"
);

drop table if exists `role`;
create table `role`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`name` varchar(20) not null comment "英文角色名",
	`name_zh` varchar(20) not null comment "中文角色名"
);

drop table if exists `user_role_relationship`;
create table `user_role_relationship`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`user_id` int not null comment "用户id",
	`role_id` int not null comment "角色id"
);

insert into user(`username`,`password`,`nickname`,`email`,`create_time`,`update_time`) values ("admin","$2a$10$PbVanJ6b5WCJT.d5s9ztS.AZBLp8exY.bhQMDUVd7ql4/GOaS8G1a","管理员","2425235803@qq.com",now(),now());
insert into user(`username`,`password`,`nickname`,`email`,`create_time`,`update_time`) values ("user","$2a$10$mZedJKMamVUuTK5ODtbXF.UusRQanWUBHSpHdZbrmsCWn9VYs2tFW","用户","2395830314@qq.com",now(),now());

insert into role(`name`,`name_zh`) values ("ROLE_ADMIN","管理员");
insert into role(`name`,`name_zh`) values ("ROLE_READER","读者");

insert into user_role_relationship(`user_id`,`role_id`) values (1,1);
insert into user_role_relationship(`user_id`,`role_id`) values (2,2);

drop table if exists `post`;
create table `post`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`title` text not null comment "标题",
	`user_id` int not null comment "用户id",
	`first_picture` varchar(500) not null default("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3317240047,2073069235&fm=11&gp=0.jpg") comment "首图",
	`summary` text not null comment "概述",
	`mark` int not null default(0) comment "标记",
	`content` longtext comment "正文",
	`recommend_status` tinyint(1) not null default(0) comment "推荐状态",
	`reward_status` tinyint(1) not null default(0) comment "打赏状态",
	`copyright_status` tinyint(1) not null default(0) comment "版权状态",
	`comment_status` tinyint(1) not null default(0) comment "评论状态",
	`publish_status` tinyint(1) not null default(0) comment "发布状态",
	`read_count` int not null default(0) comment "阅读量",
	`create_time` datetime not null comment "创建时间",
	`update_time` datetime not null comment "更新时间"
);

drop table if exists `comment`;
create table `comment`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`user_id` int not null comment "用户id",
	`post_id` int not null comment "文章id",
	`content` text not null comment "评论内容",
	`state` int not null default(1) comment "审核状态",
	`upvote_count` int not null default(0) comment "点赞数",
	`create_time` datetime not null comment "创建时间"
);

drop table if exists `comment_reply`;
create table `comment_reply`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`comment_id` int not null comment "被回复评论id",
	`user_id` int not null comment "用户id",
	`reply_user_id` int not null comment "被回复用户id",
	`content` text not null comment "评论内容",
	`state` int not null default(1) comment "审核状态",
	`upvote_count` int not null default(0) comment "点赞数",
	`create_time` datetime not null comment "创建时间"
);

drop table if exists `label`;
create table `label`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`label_name` varchar(20) not null comment "标签名"
);

drop table if exists `post_label_relationship`;
create table `post_label_relationship`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`post_id` int not null comment "文章id",
	`label_id` int not null comment "标签id"
);

drop table if exists `clazz`;
create table `clazz`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`clazz_name` varchar(20) not null comment "分类名"
);

drop table if exists `post_clazz_relationship`;
create table `post_clazz_relationship`(
	`id` int auto_increment not null primary key comment "唯一索引",
	`post_id` int not null comment "文章id",
	`clazz_id` int not null comment "分类id"
);
```
## 3.3 启动项目
直接点击IDEA中启动按钮，如下：
![启动按钮](https://img-blog.csdnimg.cn/20210304223754802.png)
然后在浏览器中输入[http://localhost:8080/](http://localhost:8080/)，即可访问到如下页面：
![首页](https://img-blog.csdnimg.cn/20210304223943151.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1N0b25lX19GbHk=,size_16,color_FFFFFF,t_70)
给的SQL语句中没有插入博客数据，所以看到的页面会跟我的截图不太一样，大家可以访问[http://localhost:8080/admin/blog/list](http://localhost:8080/admin/blog/list)这个页面，点击图中红框里的按钮新增博客：
![博客列表](https://img-blog.csdnimg.cn/20210304224330692.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1N0b25lX19GbHk=,size_16,color_FFFFFF,t_70)
# 4、博客跳转
关于项目的详细介绍后续还会写博客，这部分暂时先空着。
# 5、写在后面
PersonalBlog还只是个原型，大家有什么问题还请指出来，有什么优化建议也请提出来。
package com.alageek.personalblog.database;

import com.alageek.personalblog.dao.UserMapper;
import com.alageek.personalblog.entity.doo.User;
import com.alageek.personalblog.entity.doo.UserExample;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class mysqlTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void userMapperTest(){
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andAccountEqualTo("admin");
        User admin = userMapper.selectByExample(example).get(0);
        System.out.println(admin);
    }

}

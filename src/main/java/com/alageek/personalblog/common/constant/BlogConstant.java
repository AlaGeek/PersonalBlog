package com.alageek.personalblog.common.constant;

/**
 * 常用字符串定义
 * @author AlaGeek
 */
public class BlogConstant {

    public static final String ATTR_NAME_URL = "url";
    public static final String ATTR_NAME_EXCEPTION = "exception";
    public static final String ATTR_NAME_MESSAGE = "message";
    public static final String ATTR_NAME_CLAZZ = "clazz";
    public static final String ATTR_NAME_CLAZZ_LIST = "clazzList";
    public static final String ATTR_NAME_LABEL = "label";
    public static final String ATTR_NAME_LABEL_LIST = "labelList";
    public static final String ATTR_NAME_POST = "post";
    public static final String ATTR_NAME_POST_LIST = "postList";
    public static final String ATTR_NAME_NEW_POST_LIST = "newPostList";
    public static final String ATTR_NAME_COUNT = "count";
    public static final String ATTR_NAME_CLAZZ_ID = "clazzId";
    public static final String ATTR_NAME_LABEL_ID = "labelId";
    public static final String ATTR_NAME_KEYWORD = "keyword";
    public static final String ATTR_NAME_COMMENT_LIST = "commentList";
}

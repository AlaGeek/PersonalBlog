package com.alageek.personalblog.web;

import com.alageek.personalblog.common.constant.BlogConstant;
import com.alageek.personalblog.entity.doo.Post;
import com.alageek.personalblog.entity.vo.ClazzVo;
import com.alageek.personalblog.entity.vo.LabelVo;
import com.alageek.personalblog.entity.vo.PostVo;
import com.alageek.personalblog.service.ClazzService;
import com.alageek.personalblog.service.LabelService;
import com.alageek.personalblog.service.PostService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页控制器
 * @author AlaGeek
 */
@Slf4j
@Controller
public class IndexController {

    private final PostService postService;
    private final ClazzService clazzService;
    private final LabelService labelService;

    public IndexController(PostService postService, ClazzService clazzService, LabelService labelService) {
        this.postService = postService;
        this.clazzService = clazzService;
        this.labelService = labelService;
    }

    @GetMapping("/")
    public String index(
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
            ModelMap modelMap) {
        List<ClazzVo> clazzVoList = clazzService.countPostGroupByClazzId(6);
        modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ_LIST, clazzVoList);
        List<LabelVo> labelVoList = labelService.countPostGroupByLabelId(10);
        modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL_LIST, labelVoList);
        List<Post> postList = postService.findNewRecommendPost(6);
        modelMap.addAttribute(BlogConstant.ATTR_NAME_NEW_POST_LIST, postList);
        if (pageNum > 0 && pageSize > 0) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<PostVo> postVoList = postService.findByParamWithUser(new Post());
        modelMap.addAttribute(BlogConstant.ATTR_NAME_POST_LIST, new PageInfo<PostVo>(postVoList));
        return "index";
    }

    @GetMapping("/search")
    public String search(
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
            @RequestParam(value = "keyword", defaultValue = "") String keyword,
            ModelMap modelMap) {
        if (pageNum > 0 && pageSize > 0) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<PostVo> postVoList = postService.findByKeywordWithUser(keyword);
        if (postVoList == null) {
            postVoList = new ArrayList<>();
        }
        modelMap.addAttribute(BlogConstant.ATTR_NAME_KEYWORD, keyword);
        modelMap.addAttribute(BlogConstant.ATTR_NAME_COUNT, postVoList.size());
        modelMap.addAttribute(BlogConstant.ATTR_NAME_POST_LIST, new PageInfo<PostVo>(postVoList));
        return "search";
    }

}

package com.alageek.personalblog.web;

import com.alageek.personalblog.common.constant.BlogConstant;
import com.alageek.personalblog.entity.doo.Clazz;
import com.alageek.personalblog.service.ClazzService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * 分类管理控制器
 * @author AlaGeek
 */
@Slf4j
@Controller
@RequestMapping("/admin/type")
public class AdminTypeController {

    private final ClazzService clazzService;

    public AdminTypeController(ClazzService clazzService) {
        this.clazzService = clazzService;
    }

    @GetMapping("/list")
    public String list(
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            ModelMap modelMap) {
        if (pageNum > 0 && pageSize > 0) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<Clazz> clazzList = clazzService.findAllSortByIdDesc();
        modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ_LIST, new PageInfo<Clazz>(clazzList));
        return "admin/type-list";
    }

    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ, new Clazz());
        return "admin/type-add";
    }

    @PostMapping("/add")
    public String doAdd(Clazz clazz, ModelMap modelMap, RedirectAttributes attributes) {
        log.debug("Add clazz: {}", clazz);
        // 参数校验
        if (clazz.getClazzName() == null || clazz.getClazzName().trim().length() == 0) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ, clazz);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "分类名称不能为空");
            return "admin/type-add";
        }
        // 分类名称判重
        List<Clazz> repeatClazzList = clazzService.findByParam(clazz);
        if (repeatClazzList != null && repeatClazzList.size() > 0 && repeatClazzList.get(0) != null) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ, clazz);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "分类名称不能重复");
            return "admin/type-add";
        }
        // 新增
        int result = clazzService.saveSelective(clazz);
        if (result == 1) {
            // 新增成功
            log.debug("Add clazz success");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "新增成功");
        } else {
            // 新增失败
            log.debug("Add clazz fail");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "新增失败");
        }
        return "redirect:/admin/type/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap modelMap, RedirectAttributes attributes) {
        Clazz clazz = clazzService.findById(id);
        if (clazz == null) {
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "没有该分类，无法编辑");
            return "redirect:/admin/type/list";
        } else {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ, clazz);
            return "admin/type-edit";
        }
    }

    @PostMapping("/edit")
    public String doEdit(Clazz clazz, ModelMap modelMap, RedirectAttributes attributes) {
        log.debug("Edit clazz: {}", clazz);
        // 参数校验
        if (clazz.getClazzName() == null || clazz.getClazzName().trim().length() == 0) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ, clazz);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "分类名称不能为空");
            return "admin/type-edit";
        }
        // 分类名称判重
        List<Clazz> repeatClazzList = clazzService.findByParam(new Clazz(null, clazz.getClazzName()));
        if (repeatClazzList != null && repeatClazzList.size() > 0 && repeatClazzList.get(0) != null) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ, clazz);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "分类名称不能重复");
            return "admin/type-edit";
        }
        // 编辑
        int result = clazzService.modifyByIdSelective(clazz);
        if (result == 1) {
            // 编辑成功
            log.debug("Edit clazz success");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "编辑成功");
        } else {
            // 编辑失败
            log.debug("Edit clazz fail");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "编辑失败");
        }
        return "redirect:/admin/type/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id, RedirectAttributes attributes) {
        // 删除
        int result = clazzService.removeById(id);
        if (result == 1) {
            // 删除成功
            log.debug("Edit clazz success");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "删除成功");
        } else {
            // 删除失败
            log.debug("Edit clazz fail");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "删除失败");
        }
        return "redirect:/admin/type/list";
    }

}

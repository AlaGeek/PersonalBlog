package com.alageek.personalblog.web;

import com.alageek.personalblog.common.constant.BlogConstant;
import com.alageek.personalblog.entity.doo.Label;
import com.alageek.personalblog.service.LabelService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * 标签管理控制器
 * @author AlaGeek
 */
@Slf4j
@Controller
@RequestMapping("/admin/tag")
public class AdminTagController {

    private final LabelService labelService;

    public AdminTagController(LabelService labelService) {
        this.labelService = labelService;
    }

    @GetMapping("/list")
    public String list(
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            ModelMap modelMap) {
        if (pageNum > 0 && pageSize > 0) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<Label> labelList = labelService.findAllSortByIdDesc();
        modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL_LIST, new PageInfo<Label>(labelList));
        return "admin/tag-list";
    }

    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL, new Label());
        return "admin/tag-add";
    }

    @PostMapping("/add")
    public String doAdd(Label label, ModelMap modelMap, RedirectAttributes attributes) {
        log.debug("Add label: {}", label);
        // 参数校验
        if (label.getLabelName() == null || label.getLabelName().trim().length() == 0) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL, label);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "标签名称不能为空");
            return "admin/tag-add";
        }
        // 标签名称判重
        List<Label> repeatLabelList = labelService.findByParam(label);
        if (repeatLabelList != null && repeatLabelList.size() > 0 && repeatLabelList.get(0) != null) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL, label);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "标签名称不能重复");
            return "admin/tag-add";
        }
        // 新增
        int result = labelService.saveSelective(label);
        if (result == 1) {
            // 新增成功
            log.debug("Add label success");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "新增成功");
        } else {
            // 新增失败
            log.debug("Add label fail");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "新增失败");
        }
        return "redirect:/admin/tag/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap modelMap, RedirectAttributes attributes) {
        Label label = labelService.findById(id);
        if (label == null) {
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "没有该标签，无法编辑");
            return "redirect:/admin/tag/list";
        } else {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL, label);
            return "admin/tag-edit";
        }
    }

    @PostMapping("/edit")
    public String doEdit(Label label, ModelMap modelMap, RedirectAttributes attributes) {
        log.debug("Edit label: {}", label);
        // 参数校验
        if (label.getLabelName() == null || label.getLabelName().trim().length() == 0) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL, label);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "标签名称不能为空");
            return "admin/tag-edit";
        }
        // 标签名称判重
        List<Label> repeatLabelList = labelService.findByParam(new Label(null, label.getLabelName()));
        if (repeatLabelList != null && repeatLabelList.size() > 0 && repeatLabelList.get(0) != null) {
            modelMap.addAttribute(BlogConstant.ATTR_NAME_LABEL, label);
            modelMap.addAttribute(BlogConstant.ATTR_NAME_MESSAGE, "标签名称不能重复");
            return "admin/tag-edit";
        }
        // 编辑
        int result = labelService.modifyByIdSelective(label);
        if (result == 1) {
            // 编辑成功
            log.debug("Edit label success");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "编辑成功");
        } else {
            // 编辑失败
            log.debug("Edit label fail");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "编辑失败");
        }
        return "redirect:/admin/tag/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id, RedirectAttributes attributes) {
        // 删除
        int result = labelService.removeById(id);
        if (result == 1) {
            // 删除成功
            log.debug("Edit label success");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "删除成功");
        } else {
            // 删除失败
            log.debug("Edit label fail");
            attributes.addFlashAttribute(BlogConstant.ATTR_NAME_MESSAGE, "删除失败");
        }
        return "redirect:/admin/tag/list";
    }

}

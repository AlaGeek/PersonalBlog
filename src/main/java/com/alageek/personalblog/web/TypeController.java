package com.alageek.personalblog.web;

import com.alageek.personalblog.common.constant.BlogConstant;
import com.alageek.personalblog.entity.query.PostQuery;
import com.alageek.personalblog.entity.vo.ClazzVo;
import com.alageek.personalblog.entity.vo.PostVo;
import com.alageek.personalblog.service.ClazzService;
import com.alageek.personalblog.service.PostService;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * 分类控制器
 * @author AlaGeek
 */
@Slf4j
@Controller
@RequestMapping("/type")
public class TypeController {

    private final ClazzService clazzService;
    private final PostService postService;

    public TypeController(ClazzService clazzService, PostService postService) {
        this.clazzService = clazzService;
        this.postService = postService;
    }

    @GetMapping("/list")
    public String list(
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", defaultValue = "3") Integer pageSize,
            @RequestParam(value = "clazzId", defaultValue = "-1") Integer clazzId,
            ModelMap modelMap) {
        // 参数校验（待写）
        // 获取所有类型
        List<ClazzVo> clazzVoList = clazzService.countPostGroupByClazzId(-1);
        if (clazzVoList == null) {
            clazzVoList = new ArrayList<>();
        }
        modelMap.addAttribute(BlogConstant.ATTR_NAME_COUNT, clazzVoList.size());
        modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ_LIST, clazzVoList);
        // 获取clazzId对应的博客
        if (pageNum > 0 && pageSize > 0) {
            PageHelper.startPage(pageNum, pageSize);
        }
        PostQuery postQuery = new PostQuery();
        if (clazzId == -1) {
            if (clazzVoList.size() > 0 && clazzVoList.get(0) != null) {
                clazzId = clazzVoList.get(0).getId();
            }
        }
        postQuery.setClazzId(clazzId);
        List<PostVo> postVoList = postService.findByParamWithClazz(postQuery);
        modelMap.addAttribute(BlogConstant.ATTR_NAME_POST_LIST, postVoList);
        modelMap.addAttribute(BlogConstant.ATTR_NAME_CLAZZ_ID, clazzId);
        return "type-list";
    }

}

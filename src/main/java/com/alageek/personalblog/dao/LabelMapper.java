package com.alageek.personalblog.dao;

import com.alageek.personalblog.entity.doo.Label;
import com.alageek.personalblog.entity.doo.LabelExample;
import com.alageek.personalblog.entity.vo.LabelVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * label表的crud
 * @author AlaGeek
 */
@Mapper
public interface LabelMapper {
    long countByExample(LabelExample example);

    int deleteByExample(LabelExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Label record);

    int insertSelective(Label record);

    List<Label> selectByExample(LabelExample example);

    Label selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Label record, @Param("example") LabelExample example);

    int updateByExample(@Param("record") Label record, @Param("example") LabelExample example);

    int updateByPrimaryKeySelective(Label record);

    int updateByPrimaryKey(Label record);

    int insertPostLabelRelationship(@Param("postId") Integer postId, @Param("labelId") Integer labelId);

    List<Integer> selectLabelIdsByPostId(Integer postId);

    List<String> selectLabelNamesByPostId(Integer postId);

    int deleteLabelIdsByPostId(Integer postId);

    List<LabelVo> countPostGroupByLabelId(Integer amount);
}
package com.alageek.personalblog.dao;

import com.alageek.personalblog.entity.doo.Post;
import com.alageek.personalblog.entity.doo.PostExample;
import com.alageek.personalblog.entity.vo.PostVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * post表的crud
 * @author AlaGeek
 */
@Mapper
public interface PostMapper {
    long countByExample(PostExample example);

    int deleteByExample(PostExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Post record);

    int insertSelective(Post record);

    List<Post> selectByExample(PostExample example);

    Post selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Post record, @Param("example") PostExample example);

    int updateByExample(@Param("record") Post record, @Param("example") PostExample example);

    int updateByPrimaryKeySelective(Post record);

    int updateByPrimaryKey(Post record);

    List<PostVo> selectByExampleWithClazz(@Param("example") PostExample example, @Param("clazzId") Integer clazzId);

    List<PostVo> selectByExampleWithLabel(@Param("example") PostExample example, @Param("labelId") Integer labelId);

    List<PostVo> selectByExampleWithUser(PostExample example);

    List<Post> selectNewRecommendPost(Integer amount);
}
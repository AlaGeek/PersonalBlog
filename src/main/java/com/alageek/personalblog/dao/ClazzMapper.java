package com.alageek.personalblog.dao;

import com.alageek.personalblog.entity.doo.Clazz;
import com.alageek.personalblog.entity.doo.ClazzExample;
import com.alageek.personalblog.entity.vo.ClazzVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * clazz表的crud
 * @author AlaGeek
 */
@Mapper
public interface ClazzMapper {
    long countByExample(ClazzExample example);

    int deleteByExample(ClazzExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Clazz record);

    int insertSelective(Clazz record);

    List<Clazz> selectByExample(ClazzExample example);

    Clazz selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Clazz record, @Param("example") ClazzExample example);

    int updateByExample(@Param("record") Clazz record, @Param("example") ClazzExample example);

    int updateByPrimaryKeySelective(Clazz record);

    int updateByPrimaryKey(Clazz record);

    int insertPostClazzRelationship(@Param("postId") Integer postId, @Param("clazzId") Integer clazzId);

    List<Integer> selectClazzIdsByPostId(Integer postId);

    int deleteClazzIdsByPostId(Integer postId);

    List<ClazzVo> countPostGroupByClazzId(Integer amount);
}
package com.alageek.personalblog.dao;

import com.alageek.personalblog.entity.doo.CommentReply;
import com.alageek.personalblog.entity.doo.CommentReplyExample;
import com.alageek.personalblog.entity.vo.CommentReplyVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * comment_reply表的crud
 * @author AlaGeek
 */
@Mapper
public interface CommentReplyMapper {
    long countByExample(CommentReplyExample example);

    int deleteByExample(CommentReplyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CommentReply record);

    int insertSelective(CommentReply record);

    List<CommentReply> selectByExample(CommentReplyExample example);

    CommentReply selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CommentReply record, @Param("example") CommentReplyExample example);

    int updateByExample(@Param("record") CommentReply record, @Param("example") CommentReplyExample example);

    int updateByPrimaryKeySelective(CommentReply record);

    int updateByPrimaryKey(CommentReply record);

    List<CommentReplyVo> selectByCommentId(Integer commentId);
}
package com.alageek.personalblog.dao;

import com.alageek.personalblog.entity.doo.Role;
import com.alageek.personalblog.entity.doo.RoleExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * role表的crud
 * @author AlaGeek
 */
@Mapper
public interface RoleMapper {

    long countByExample(RoleExample example);

    int deleteByExample(RoleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    List<Role> selectByExample(RoleExample example);

    Role selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Role record, @Param("example") RoleExample example);

    int updateByExample(@Param("record") Role record, @Param("example") RoleExample example);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    List<Role> selectByUserId(Integer userId);

    int insertUserRoleRelationship(@Param("userId") Integer userId, @Param("roleId") Integer roleId);
}

package com.alageek.personalblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * @author AlaGeek
 */
@SpringBootApplication
public class PersonalblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalblogApplication.class, args);
	}

}

package com.alageek.personalblog.service;

import com.alageek.personalblog.entity.doo.Label;
import com.alageek.personalblog.entity.vo.LabelVo;

import java.util.List;

/**
 * 通用逻辑：标签服务
 * @author AlaGeek
 */
public interface LabelService {

    int saveSelective(Label label);

    int removeById(Integer id);

    int modifyByIdSelective(Label label);

    Label findById(Integer id);

    List<Label> findByParam(Label label);

    List<Label> findAllSortByIdDesc();

    int savePostLabelRelationship(Integer postId, Integer labelId);

    List<Integer> findLabelIdsByPostId(Integer postId);

    List<String> findLabelNamesByPostId(Integer postId);

    int removeLabelIdsByPostId(Integer postId);

    List<LabelVo> countPostGroupByLabelId(Integer amount);

}

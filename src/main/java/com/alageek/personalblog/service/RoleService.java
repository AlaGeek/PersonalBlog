package com.alageek.personalblog.service;

import com.alageek.personalblog.entity.doo.Role;

import java.util.List;

/**
 * 通用逻辑：角色服务
 * @author AlaGeek
 */
public interface RoleService {

    int saveSelective(Role role);

    int removeById(Integer id);

    int modifyByIdSelective(Role role);

    Role findById(Integer id);

    List<Role> findByParam(Role role);

    List<Role> findByUserId(Integer userId);

    int saveUserRoleRelationship(Integer userId, Integer roleId);

}

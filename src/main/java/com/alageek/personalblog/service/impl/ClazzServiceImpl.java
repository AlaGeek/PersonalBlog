package com.alageek.personalblog.service.impl;

import com.alageek.personalblog.dao.ClazzMapper;
import com.alageek.personalblog.entity.doo.Clazz;
import com.alageek.personalblog.entity.doo.ClazzExample;
import com.alageek.personalblog.entity.vo.ClazzVo;
import com.alageek.personalblog.service.ClazzService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用逻辑：分类服务
 * @author AlaGeek
 */
@Service
public class ClazzServiceImpl implements ClazzService {

    private final ClazzMapper clazzMapper;

    public ClazzServiceImpl(ClazzMapper clazzMapper) {
        this.clazzMapper = clazzMapper;
    }

    @Override
    public int saveSelective(Clazz clazz) {
        return clazzMapper.insertSelective(clazz);
    }

    @Override
    public int removeById(Integer id) {
        return clazzMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int modifyByIdSelective(Clazz clazz) {
        return clazzMapper.updateByPrimaryKeySelective(clazz);
    }

    @Override
    public Clazz findById(Integer id) {
        return clazzMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Clazz> findByParam(Clazz clazz) {
        ClazzExample example = new ClazzExample();
        ClazzExample.Criteria criteria = example.createCriteria();
        // 此处添加查询条件
        if(clazz.getClazzName()!=null){
            criteria.andClazzNameEqualTo(clazz.getClazzName());
        }
        return clazzMapper.selectByExample(example);
    }

    @Override
    public List<Clazz> findAllSortByIdDesc() {
        ClazzExample example = new ClazzExample();
        example.setOrderByClause("id DESC");
        return clazzMapper.selectByExample(example);
    }

    @Override
    public int savePostClazzRelationship(Integer postId, Integer clazzId) {
        return clazzMapper.insertPostClazzRelationship(postId, clazzId);
    }

    @Override
    public List<Integer> findClazzIdsByPostId(Integer postId) {
        return clazzMapper.selectClazzIdsByPostId(postId);
    }

    @Override
    public int removeClazzIdsByPostId(Integer postId){
        return clazzMapper.deleteClazzIdsByPostId(postId);
    }

    @Override
    public List<ClazzVo> countPostGroupByClazzId(Integer amount) {
        return clazzMapper.countPostGroupByClazzId(amount);
    }
}

package com.alageek.personalblog.service.impl;

import com.alageek.personalblog.dao.RoleMapper;
import com.alageek.personalblog.entity.doo.Role;
import com.alageek.personalblog.entity.doo.RoleExample;
import com.alageek.personalblog.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用逻辑：角色服务
 * @author AlaGeek
 */
@Service
public class RoleServiceImpl implements RoleService {

    private final RoleMapper roleMapper;

    public RoleServiceImpl(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }

    @Override
    public int saveSelective(Role role) {
        return roleMapper.insertSelective(role);
    }

    @Override
    public int removeById(Integer id) {
        return roleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int modifyByIdSelective(Role role) {
        return roleMapper.updateByPrimaryKeySelective(role);
    }

    @Override
    public Role findById(Integer id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Role> findByParam(Role role) {
        RoleExample example = new RoleExample();
        return roleMapper.selectByExample(example);
    }

    @Override
    public List<Role> findByUserId(Integer userId) {
        return roleMapper.selectByUserId(userId);
    }

    @Override
    public int saveUserRoleRelationship(Integer userId, Integer roleId) {
        return roleMapper.insertUserRoleRelationship(userId, roleId);
    }
}

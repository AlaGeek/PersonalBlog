package com.alageek.personalblog.service.impl;

import com.alageek.personalblog.dao.LabelMapper;
import com.alageek.personalblog.entity.doo.Label;
import com.alageek.personalblog.entity.doo.LabelExample;
import com.alageek.personalblog.entity.vo.LabelVo;
import com.alageek.personalblog.service.LabelService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用逻辑：标签服务
 * @author AlaGeek
 */
@Service
public class LabelServiceImpl implements LabelService {

    private final LabelMapper labelMapper;

    public LabelServiceImpl(LabelMapper labelMapper) {
        this.labelMapper = labelMapper;
    }

    @Override
    public int saveSelective(Label label) {
        return labelMapper.insertSelective(label);
    }

    @Override
    public int removeById(Integer id) {
        return labelMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int modifyByIdSelective(Label label) {
        return labelMapper.updateByPrimaryKeySelective(label);
    }

    @Override
    public Label findById(Integer id) {
        return labelMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Label> findByParam(Label label) {
        LabelExample example = new LabelExample();
        LabelExample.Criteria criteria = example.createCriteria();
        // 此处添加查询条件
        if (label.getLabelName() != null) {
            criteria.andLabelNameEqualTo(label.getLabelName());
        }
        return labelMapper.selectByExample(example);
    }

    @Override
    public List<Label> findAllSortByIdDesc() {
        LabelExample example = new LabelExample();
        example.setOrderByClause("id DESC");
        return labelMapper.selectByExample(example);
    }

    @Override
    public int savePostLabelRelationship(Integer postId, Integer labelId) {
        return labelMapper.insertPostLabelRelationship(postId, labelId);
    }

    @Override
    public List<Integer> findLabelIdsByPostId(Integer postId) {
        return labelMapper.selectLabelIdsByPostId(postId);
    }

    @Override
    public List<String> findLabelNamesByPostId(Integer postId) {
        return labelMapper.selectLabelNamesByPostId(postId);
    }

    @Override
    public int removeLabelIdsByPostId(Integer postId) {
        return labelMapper.deleteLabelIdsByPostId(postId);
    }

    @Override
    public List<LabelVo> countPostGroupByLabelId(Integer amount) {
        return labelMapper.countPostGroupByLabelId(amount);
    }

}

package com.alageek.personalblog.service.impl;

import com.alageek.personalblog.dao.CommentMapper;
import com.alageek.personalblog.entity.doo.Comment;
import com.alageek.personalblog.entity.doo.CommentExample;
import com.alageek.personalblog.entity.vo.CommentVo;
import com.alageek.personalblog.service.CommentService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用逻辑：评论服务
 * @author AlaGeek
 */
@Service
public class CommentServiceImpl implements CommentService {

    private final CommentMapper commentMapper;

    public CommentServiceImpl(CommentMapper commentMapper) {
        this.commentMapper = commentMapper;
    }

    @Override
    public int saveSelective(Comment comment) {
        return commentMapper.insertSelective(comment);
    }

    @Override
    public int removeById(Integer id) {
        return commentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int modifyByIdSelective(Comment comment) {
        return commentMapper.updateByPrimaryKeySelective(comment);
    }

    @Override
    public Comment findById(Integer id) {
        return commentMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Comment> findByParam(Comment comment) {
        CommentExample example = new CommentExample();
        return commentMapper.selectByExample(example);
    }

    @Override
    public List<CommentVo> findByPostId(Integer postId) {
        return commentMapper.selectByPostId(postId);
    }
}

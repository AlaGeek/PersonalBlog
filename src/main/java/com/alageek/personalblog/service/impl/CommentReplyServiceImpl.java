package com.alageek.personalblog.service.impl;

import com.alageek.personalblog.dao.CommentReplyMapper;
import com.alageek.personalblog.entity.doo.CommentReply;
import com.alageek.personalblog.entity.doo.CommentReplyExample;
import com.alageek.personalblog.entity.vo.CommentReplyVo;
import com.alageek.personalblog.service.CommentReplyService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用逻辑：评论回复服务
 * @author AlaGeek
 */
@Service
public class CommentReplyServiceImpl implements CommentReplyService {

    private final CommentReplyMapper commentReplyMapper;

    public CommentReplyServiceImpl(CommentReplyMapper commentReplyMapper) {
        this.commentReplyMapper = commentReplyMapper;
    }

    @Override
    public int saveSelective(CommentReply commentReply) {
        return commentReplyMapper.insertSelective(commentReply);
    }

    @Override
    public int removeById(Integer id) {
        return commentReplyMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int modifyByIdSelective(CommentReply commentReply) {
        return commentReplyMapper.updateByPrimaryKeySelective(commentReply);
    }

    @Override
    public CommentReply findById(Integer id) {
        return commentReplyMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CommentReply> findByParam(CommentReply commentReply) {
        CommentReplyExample example = new CommentReplyExample();
        return commentReplyMapper.selectByExample(example);
    }

    @Override
    public List<CommentReplyVo> findByCommentId(Integer commentId) {
        return commentReplyMapper.selectByCommentId(commentId);
    }
}

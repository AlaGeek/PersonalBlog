package com.alageek.personalblog.service.impl;

import com.alageek.personalblog.dao.PostMapper;
import com.alageek.personalblog.entity.doo.Post;
import com.alageek.personalblog.entity.doo.PostExample;
import com.alageek.personalblog.entity.query.PostQuery;
import com.alageek.personalblog.entity.vo.PostVo;
import com.alageek.personalblog.service.PostService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用逻辑：博文服务
 * @author AlaGeek
 */
@Service
public class PostServiceImpl implements PostService {

    private final PostMapper postMapper;

    public PostServiceImpl(PostMapper postMapper) {
        this.postMapper = postMapper;
    }

    @Override
    public int saveSelective(Post post) {
        return postMapper.insertSelective(post);
    }

    @Override
    public int removeById(Integer id) {
        return postMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int modifyByIdSelective(Post post) {
        return postMapper.updateByPrimaryKeySelective(post);
    }

    @Override
    public Post findById(Integer id) {
        return postMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Post> findByParam(Post post) {
        PostExample example = new PostExample();
        PostExample.Criteria criteria = example.createCriteria();
        // 此处添加查询条件
        if (post.getTitle() != null && post.getTitle().length() > 0) {
            criteria.andTitleEqualTo(post.getTitle());
        }
        if (post.getMark() != null) {
            criteria.andMarkEqualTo(post.getMark());
        }
        if (post.getRecommendStatus() != null) {
            criteria.andRecommendStatusEqualTo(post.getRecommendStatus());
        }
        if (post.getRewardStatus() != null) {
            criteria.andRewardStatusEqualTo(post.getRewardStatus());
        }
        if (post.getCopyrightStatus() != null) {
            criteria.andCopyrightStatusEqualTo(post.getCopyrightStatus());
        }
        if (post.getCommentStatus() != null) {
            criteria.andCommentStatusEqualTo(post.getCommentStatus());
        }
        if (post.getPublishStatus() != null) {
            criteria.andPublishStatusEqualTo(post.getPublishStatus());
        }
        // 按照更新时间倒序
        example.setOrderByClause("update_time DESC");
        return postMapper.selectByExample(example);
    }

    @Override
    public List<PostVo> findByParamWithClazz(PostQuery postQuery) {
        PostExample example = new PostExample();
        PostExample.Criteria criteria = example.createCriteria();
        // 此处添加查询条件
        if (postQuery.getTitle() != null && postQuery.getTitle().length() > 0) {
            criteria.andTitleLike("%" + postQuery.getTitle() + "%");
        }
        if (postQuery.getMark() != null) {
            criteria.andMarkEqualTo(postQuery.getMark());
        }
        if (postQuery.getRecommendStatus() != null) {
            criteria.andRecommendStatusEqualTo(postQuery.getRecommendStatus());
        }
        if (postQuery.getRewardStatus() != null) {
            criteria.andRewardStatusEqualTo(postQuery.getRewardStatus());
        }
        if (postQuery.getCopyrightStatus() != null) {
            criteria.andCopyrightStatusEqualTo(postQuery.getCopyrightStatus());
        }
        if (postQuery.getCommentStatus() != null) {
            criteria.andCommentStatusEqualTo(postQuery.getCommentStatus());
        }
        if (postQuery.getPublishStatus() != null) {
            criteria.andPublishStatusEqualTo(postQuery.getPublishStatus());
        }
        // 按照更新时间倒序
        example.setOrderByClause("update_time DESC");
        return postMapper.selectByExampleWithClazz(example, postQuery.getClazzId());
    }

    @Override
    public List<PostVo> findByParamWithLabel(PostQuery postQuery) {
        PostExample example = new PostExample();
        PostExample.Criteria criteria = example.createCriteria();
        // 此处添加查询条件
        if (postQuery.getTitle() != null && postQuery.getTitle().length() > 0) {
            criteria.andTitleLike("%" + postQuery.getTitle() + "%");
        }
        if (postQuery.getMark() != null) {
            criteria.andMarkEqualTo(postQuery.getMark());
        }
        if (postQuery.getRecommendStatus() != null) {
            criteria.andRecommendStatusEqualTo(postQuery.getRecommendStatus());
        }
        if (postQuery.getRewardStatus() != null) {
            criteria.andRewardStatusEqualTo(postQuery.getRewardStatus());
        }
        if (postQuery.getCopyrightStatus() != null) {
            criteria.andCopyrightStatusEqualTo(postQuery.getCopyrightStatus());
        }
        if (postQuery.getCommentStatus() != null) {
            criteria.andCommentStatusEqualTo(postQuery.getCommentStatus());
        }
        if (postQuery.getPublishStatus() != null) {
            criteria.andPublishStatusEqualTo(postQuery.getPublishStatus());
        }
        // 按照更新时间倒序
        example.setOrderByClause("update_time DESC");
        return postMapper.selectByExampleWithLabel(example, postQuery.getLabelId());
    }

    @Override
    public List<PostVo> findByParamWithUser(Post post) {
        PostExample example = new PostExample();
        PostExample.Criteria criteria = example.createCriteria();
        // 此处添加查询条件
        if (post.getId() != null) {
            criteria.andIdEqualTo(post.getId());
        }
        if (post.getTitle() != null && post.getTitle().length() > 0) {
            criteria.andTitleEqualTo(post.getTitle());
        }
        if (post.getMark() != null) {
            criteria.andMarkEqualTo(post.getMark());
        }
        if (post.getRecommendStatus() != null) {
            criteria.andRecommendStatusEqualTo(post.getRecommendStatus());
        }
        if (post.getRewardStatus() != null) {
            criteria.andRewardStatusEqualTo(post.getRewardStatus());
        }
        if (post.getCopyrightStatus() != null) {
            criteria.andCopyrightStatusEqualTo(post.getCopyrightStatus());
        }
        if (post.getCommentStatus() != null) {
            criteria.andCommentStatusEqualTo(post.getCommentStatus());
        }
        if (post.getPublishStatus() != null) {
            criteria.andPublishStatusEqualTo(post.getPublishStatus());
        }
        // 按照更新时间倒序
        example.setOrderByClause("update_time DESC");
        return postMapper.selectByExampleWithUser(example);
    }

    @Override
    public List<Post> findNewRecommendPost(Integer amount) {
        return postMapper.selectNewRecommendPost(amount);
    }

    @Override
    public List<PostVo> findByKeywordWithUser(String keyword) {
        PostExample example = new PostExample();
        PostExample.Criteria criteriaTitle = example.createCriteria();
        if (keyword != null && keyword.trim().length() > 0) {
            criteriaTitle.andTitleLike("%" + keyword + "%");
        }
        example.or(criteriaTitle);
        PostExample.Criteria criteriaContent = example.createCriteria();
        if (keyword != null && keyword.trim().length() > 0) {
            criteriaContent.andContentLike("%" + keyword + "%");
        }
        example.or(criteriaContent);
        example.setOrderByClause("update_time DESC");
        return postMapper.selectByExampleWithUser(example);
    }
}

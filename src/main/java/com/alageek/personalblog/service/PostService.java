package com.alageek.personalblog.service;

import com.alageek.personalblog.entity.doo.Post;
import com.alageek.personalblog.entity.query.PostQuery;
import com.alageek.personalblog.entity.vo.PostVo;

import java.util.List;

/**
 * 通用逻辑：博文服务
 * @author AlaGeek
 */
public interface PostService {

    int saveSelective(Post post);

    int removeById(Integer id);

    int modifyByIdSelective(Post post);

    Post findById(Integer id);

    List<Post> findByParam(Post post);

    List<PostVo> findByParamWithClazz(PostQuery postQuery);

    List<PostVo> findByParamWithLabel(PostQuery postQuery);

    List<PostVo> findByParamWithUser(Post post);

    List<Post> findNewRecommendPost(Integer amount);

    List<PostVo> findByKeywordWithUser(String keyword);
}

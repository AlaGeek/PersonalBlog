package com.alageek.personalblog.service;

import com.alageek.personalblog.entity.doo.CommentReply;
import com.alageek.personalblog.entity.vo.CommentReplyVo;

import java.util.List;

/**
 * 通用逻辑：评论回复服务
 * @author AlaGeek
 */
public interface CommentReplyService {

    int saveSelective(CommentReply commentReply);

    int removeById(Integer id);

    int modifyByIdSelective(CommentReply commentReply);

    CommentReply findById(Integer id);

    List<CommentReply> findByParam(CommentReply commentReply);

    List<CommentReplyVo> findByCommentId(Integer commentId);

}

package com.alageek.personalblog.service;

import com.alageek.personalblog.entity.doo.Clazz;
import com.alageek.personalblog.entity.vo.ClazzVo;

import java.util.List;

/**
 * 通用逻辑：分类服务
 * @author AlaGeek
 */
public interface ClazzService {

    int saveSelective(Clazz clazz);

    int removeById(Integer id);

    int modifyByIdSelective(Clazz clazz);

    Clazz findById(Integer id);

    List<Clazz> findByParam(Clazz clazz);

    List<Clazz> findAllSortByIdDesc();

    int savePostClazzRelationship(Integer postId, Integer clazzId);

    List<Integer> findClazzIdsByPostId(Integer postId);

    int removeClazzIdsByPostId(Integer postId);

    List<ClazzVo> countPostGroupByClazzId(Integer amount);

}

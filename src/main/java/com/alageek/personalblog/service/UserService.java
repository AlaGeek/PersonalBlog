package com.alageek.personalblog.service;

import com.alageek.personalblog.entity.doo.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * 通用逻辑：用户服务
 * @author AlaGeek
 */
public interface UserService extends UserDetailsService {

    int saveSelective(User user);

    int removeById(Integer id);

    int modifyByIdSelective(User user);

    User findById(Integer id);

    List<User> findByParam(User user);

}

package com.alageek.personalblog.service;

import com.alageek.personalblog.entity.doo.Comment;
import com.alageek.personalblog.entity.vo.CommentVo;

import java.util.List;

/**
 * 通用逻辑：评论服务
 * @author AlaGeek
 */
public interface CommentService {

    int saveSelective(Comment comment);

    int removeById(Integer id);

    int modifyByIdSelective(Comment comment);

    Comment findById(Integer id);

    List<Comment> findByParam(Comment comment);

    List<CommentVo> findByPostId(Integer postId);

}

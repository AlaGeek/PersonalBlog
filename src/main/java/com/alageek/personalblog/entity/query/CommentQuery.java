package com.alageek.personalblog.entity.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 查询类：评论
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentQuery {

    private Integer id;
    private Integer commentId;
    private Integer userId;
    private Integer postId;
    private Integer replyUserId;
    private String content;
    private Integer state;
    private Integer upvoteCount;
    private Date createTime;

}

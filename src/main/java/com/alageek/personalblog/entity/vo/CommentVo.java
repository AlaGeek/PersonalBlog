package com.alageek.personalblog.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 视图类：评论
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentVo {

    private Integer id;
    private Integer userId;
    private String nickname;
    private String picture;
    private Integer postId;
    private String content;
    private Integer state;
    private Integer upvoteCount;
    private Date createTime;
    List<CommentReplyVo> commentReplyVoList;

}

package com.alageek.personalblog.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 视图类：博文
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostVo{

    private Integer id;
    private String title;
    private Integer userId;
    private String firstPicture;
    private String summary;
    private Integer mark;
    private String content;
    private Boolean recommendStatus;
    private Boolean rewardStatus;
    private Boolean copyrightStatus;
    private Boolean commentStatus;
    private Boolean publishStatus;
    private Integer readCount;
    private Date createTime;
    private Date updateTime;
    private Integer clazzId;
    private String clazzName;
    private String nickname;
    private String picture;
    private List<String> labelNameList;

}

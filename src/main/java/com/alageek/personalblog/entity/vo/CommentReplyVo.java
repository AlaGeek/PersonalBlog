package com.alageek.personalblog.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 视图类：评论回复
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentReplyVo {

    private Integer id;
    private Integer commentId;
    private Integer userId;
    private String nickname;
    private String picture;
    private Integer replyUserId;
    private String content;
    private Integer state;
    private Integer upvoteCount;
    private Date createTime;

}
package com.alageek.personalblog.entity.vo;

import com.alageek.personalblog.entity.doo.Role;
import com.alageek.personalblog.entity.doo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 视图类：用户
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVo implements UserDetails {

    private static final long serialVersionUID = 3097501819592801226L;
    private Integer id;
    private String account;
    private String password;
    private String nickname;
    private String email;
    private String picture;
    private Boolean enabled;
    private Date createTime;
    private Date updateTime;
    private List<Role> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return account;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public UserVo(User user) {
        this.id = user.getId();
        this.account = user.getAccount();
        this.password = user.getPassword();
        this.nickname = user.getNickname();
        this.email = user.getEmail();
        this.picture = user.getPicture();
        this.enabled = user.getEnabled();
        this.createTime = user.getCreateTime();
        this.updateTime = user.getUpdateTime();
    }

}

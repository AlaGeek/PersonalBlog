package com.alageek.personalblog.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 视图类：标签
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LabelVo {

    private Integer id;
    private String labelName;
    private Integer postCount;

}

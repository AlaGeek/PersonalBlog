package com.alageek.personalblog.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 视图类：分类
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClazzVo {

    private Integer id;
    private String clazzName;
    private Integer postCount;

}

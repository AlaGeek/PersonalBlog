package com.alageek.personalblog.entity.doo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实体类：标签
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Label {

    private Integer id;
    private String labelName;

}
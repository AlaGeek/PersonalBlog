package com.alageek.personalblog.entity.doo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 实体类：用户
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Integer id;
    private String account;
    private String password;
    private String nickname;
    private String email;
    private String picture;
    private Boolean enabled;
    private Date createTime;
    private Date updateTime;

}

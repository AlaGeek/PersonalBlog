package com.alageek.personalblog.entity.doo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 实体类：角色
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role implements Serializable {

    private static final long serialVersionUID = -8967355545590741575L;
    private Integer id;
    private String name;
    private String nameZh;

}

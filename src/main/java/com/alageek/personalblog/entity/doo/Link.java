package com.alageek.personalblog.entity.doo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实体类：友情链接
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Link {

    private Integer id;
    private String linkName;
    private String linkUrl;
    private String logo;

}
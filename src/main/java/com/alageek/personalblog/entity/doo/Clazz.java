package com.alageek.personalblog.entity.doo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实体类：分类
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Clazz {

    private Integer id;
    private String clazzName;

}
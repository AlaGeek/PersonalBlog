package com.alageek.personalblog.entity.doo;

import com.alageek.personalblog.entity.query.CommentQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 实体类：评论回复
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentReply {

    private Integer id;
    private Integer commentId;
    private Integer userId;
    private Integer replyUserId;
    private String content;
    private Integer state;
    private Integer upvoteCount;
    private Date createTime;

    public CommentReply(CommentQuery commentQuery){
        this.id = commentQuery.getId();
        this.commentId = commentQuery.getCommentId();
        this.userId = commentQuery.getUserId();
        this.replyUserId = commentQuery.getReplyUserId();
        this.content = commentQuery.getContent();
        this.state = commentQuery.getState();
        this.upvoteCount = commentQuery.getUpvoteCount();
        this.createTime = commentQuery.getCreateTime();
    }

}
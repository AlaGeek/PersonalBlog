package com.alageek.personalblog.entity.doo;

import com.alageek.personalblog.entity.query.PostQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 实体类：博文
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    private Integer id;
    private String title;
    private Integer userId;
    private String firstPicture;
    private String summary;
    private Integer mark;
    private String content;
    private Boolean recommendStatus;
    private Boolean rewardStatus;
    private Boolean copyrightStatus;
    private Boolean commentStatus;
    private Boolean publishStatus;
    private Integer readCount;
    private Date createTime;
    private Date updateTime;

    public Post(PostQuery postQuery) {
        this.id = postQuery.getId();
        this.title = postQuery.getTitle();
        this.userId = postQuery.getUserId();
        this.firstPicture = postQuery.getFirstPicture();
        this.summary = postQuery.getSummary();
        this.mark = postQuery.getMark();
        this.content = postQuery.getContent();
        this.recommendStatus = postQuery.getRecommendStatus();
        this.rewardStatus = postQuery.getRewardStatus();
        this.copyrightStatus = postQuery.getCopyrightStatus();
        this.commentStatus = postQuery.getCommentStatus();
        this.publishStatus = postQuery.getPublishStatus();
        this.readCount = postQuery.getReadCount();
        this.createTime = postQuery.getCreateTime();
        this.updateTime = postQuery.getUpdateTime();
    }

}
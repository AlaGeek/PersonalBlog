package com.alageek.personalblog.entity.doo;

import com.alageek.personalblog.entity.query.CommentQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 实体类：评论
 * @author AlaGeek
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

    private Integer id;
    private Integer userId;
    private Integer postId;
    private String content;
    private Integer state;
    private Integer upvoteCount;
    private Date createTime;

    public Comment(CommentQuery commentQuery){
        this.id = commentQuery.getId();
        this.userId = commentQuery.getUserId();
        this.postId = commentQuery.getPostId();
        this.content = commentQuery.getContent();
        this.state = commentQuery.getState();
        this.upvoteCount = commentQuery.getUpvoteCount();
        this.createTime = commentQuery.getCreateTime();
    }

}